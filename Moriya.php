<?php

namespace App\_Moriya;

use Illuminate\Support\Facades\Facade;

class Moriya extends Facade {

    /**
     * ファサード呼び出し時にどのサービスプロバイダーを呼び出すか
     */
    public static function getFacadeAccessor()
    {
        return 'moriya';
    }

}