<?php

namespace App\_Moriya;

class MoriyaProvider
{
    private $files = [];

    public function __construct()
    {
        $this->register();
    }
    /**
     * 呼び出されたクラスを名前空間から
     * 新規にエンティティを作成し返す
     * @param string $name
     * @param object $argument
     */
    public function __call($name, $argument)
    {
        require_once($this->files[$name][1]);
        $class = 'APP\_Moriya\Moriyan\\' . $this->files[$name][0];
        return new $class();
    }

    /**
     * クラスファイルを検索
     */
    private function register()
    {
        $path = dirname(__FILE__) . '/Moriyan/*.{php}' ;
        foreach (glob($path, GLOB_BRACE) as $moriya_file) {
            $this->loadClass($moriya_file);
        }
    }

    /**
     * クラスファイルパス、クラス名を保存
     */
    private function loadClass($class_path)
    {
        $_path = explode('/', $class_path);
        $Class = explode('.', end($_path));
        $this->files[$Class[0]] = [
            $Class[0], $class_path
        ];
    }
}
