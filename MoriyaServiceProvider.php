<?php

namespace App\_Moriya;

use Illuminate\Support\ServiceProvider;

class MoriyaServiceProvider extends ServiceProvider {
    public function boot()
    {
        //
    }

    /**
     * 初期化
     */
    public function register()
    {
        $this->app->bind(
            'moriya',             // キーとクラスをバインド
            'App\_Moriya\MoriyaProvider'
        );
    }
}